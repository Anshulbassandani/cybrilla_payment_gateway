function showErrorMessage(msg){
	$(".error-message")[0].innerHTML = msg;
}

function prepareAndEncryptData(sendRequest) {
	var ifsc_code = document.getElementById("ifsc").value.toString().trim();
	var bank_acc = document.getElementById("bank_acc").value.toString().trim();
	var amount = document.getElementById("amount").value.toString().trim();
	var mer_tran = document.getElementById("mer_trans").value.toString().trim();
	var trans_date = document.getElementById("trans_date").value.toString().trim();
	var pay_gate_ref = document.getElementById("mer_ref").value.toString().trim();

	if (ifsc_code === "" || bank_acc === "" || amount === "" || mer_tran === "" || trans_date === "" || pay_gate_ref === "") {
		showErrorMessage("Please fill all the Fields");
		return false;
	}

	if ( isNaN(bank_acc) || !(bank_acc.split(".").length == 1)) {
		showErrorMessage("Please Enter Correct Bank Acc. Number");
		$("#bank_acc").focus();
		return false;
	} else if(parseFloat(bank_acc) <= 0){
		showErrorMessage("Enter Valid Acc. Number");
		$("#amount").focus();
		return false;
	}

	if (isNaN(amount)) {
		showErrorMessage("Amount should be a number");
		$("#amount").focus();
		return false;
	} else if(parseFloat(amount) <= 0) {
		showErrorMessage("Enter Valid Amount");
		$("#amount").focus();
		return false;
	}

	showErrorMessage("");

	var msg = fullEncryption(ifsc_code.value, bank_acc, amount, mer_tran, trans_date, pay_gate_ref);
	if(sendRequest){
		sendPaymentRequest(msg);
		return false;
	}else{
		return msg;
	}
	return false;
}

function verifyMsg(msgString, shaHash){
	var msgHash = encryptSHA1(msgString);
	if(msgHash === shaHash){
		return true;
	}else{
		return false;
	}
}

function showResult(msg) {
	str = '<div class="form-group col-xs-12">'
	 + '<label> '+ msg +' </label>'
	 + '</div>';
		$("#form")[0].innerHTML = str;
	return;
}

function sendPaymentRequest(msg) {
	$.post("http://examplepg.com/transaction", {"msg": msg})
	.done(function(data){
		if(data){
			console.log(data);
			var response = data.msg;
			response_string = fullDecryption(response);
			var msg_arr = [];
			var msg_hash = "";
			var status_flag = false;
			var response_array = response_string.toString().split("|");
			for(i in response_array){
				var value = response_array[i].toString().trim().split("=");
				if(value[0] === "txn_status"){
					if(value[1] === "success"){
						status_flag = true;
					}
				}else if(value[0] === "hash"){
					msg_hash = value[1].toString().trim();
				}else{
					msg_arr.push(response_array[i]);
				}
			}
			if(status_flag){
				if(verifyMsg( msg_arr.join()) , msg_hash){
					showResult("Transanction Successfull");
					return;
				}else{
					showResult("Invalid Response");
					return;
				}
			}else{
				showResult("Transanction Unsuccessful. Please Try Again");
				return;
			}
		}
	})
	.fail(function(data){
		showResult("Something Went Wrong. Please Try Again");
	});
}

function fullEncryption(ifsc_code, bank_acc, amount, mer_tran_ref, trans_date, pay_gateway_mer_ref){
	var str = "bank_ifsc_code=" + ifsc_code + "|bank_account_number=" + bank_acc+ "|amount=" + amount + "|merchant_transaction_ref=" + mer_tran_ref+ "|transaction_date="+trans_date+"|payment_gateway_merchant_reference="+pay_gateway_mer_ref;

	// To SHA
	var shaEnc = encryptSHA1(str).toString();
	var payload_with_sha = str + "\|hash\=" + shaEnc;

	// To AES
	var key = "Q9fbkBF8au24C9wshGRW9ut8ecYpyXye5vhFLtHFdGjRg3a4HxPYRfQaKutZx5N4";
	payload_to_pg = encryptAES(payload_with_sha, key);
	payload_to_pg_text = payload_to_pg.toString(CryptoJS.enc.Utf8);

	// To Base64
	var msg = encryptBase64(payload_to_pg, "");
	
	return msg;
}

function fullDecryption(cypherText){
	// Decrypting Base64
	var payload_to_pg = decryptBase64(cypherText);

	// Decrypting AES
	var key = "Q9fbkBF8au24C9wshGRW9ut8ecYpyXye5vhFLtHFdGjRg3a4HxPYRfQaKutZx5N4";
	var payload_with_sha = decryptAES(payload_to_pg, key);

	// console.log(payload_with_sha);

	return payload_with_sha;
}

function getBase64(key){

	var Base64 = {

		// private property
		_keyStr : key,

		// public method for encoding
		encode : function (input) {
		    var output = "";
		    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		    var i = 0;

		    input = Base64._utf8_encode(input);

		    while (i < input.length) {

		        chr1 = input.charCodeAt(i++);
		        chr2 = input.charCodeAt(i++);
		        chr3 = input.charCodeAt(i++);

		        enc1 = chr1 >> 2;
		        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
		        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
		        enc4 = chr3 & 63;

		        if (isNaN(chr2)) {
		            enc3 = enc4 = 64;
		        } else if (isNaN(chr3)) {
		            enc4 = 64;
		        }

		        output = output +
		        this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
		        this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

		    }

		    return output;
		},

		// public method for decoding
		decode : function (input) {
		    var output = "";
		    var chr1, chr2, chr3;
		    var enc1, enc2, enc3, enc4;
		    var i = 0;

		    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		    while (i < input.length) {

		        enc1 = this._keyStr.indexOf(input.charAt(i++));
		        enc2 = this._keyStr.indexOf(input.charAt(i++));
		        enc3 = this._keyStr.indexOf(input.charAt(i++));
		        enc4 = this._keyStr.indexOf(input.charAt(i++));

		        chr1 = (enc1 << 2) | (enc2 >> 4);
		        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
		        chr3 = ((enc3 & 3) << 6) | enc4;

		        output = output + String.fromCharCode(chr1);

		        if (enc3 != 64) {
		            output = output + String.fromCharCode(chr2);
		        }
		        if (enc4 != 64) {
		            output = output + String.fromCharCode(chr3);
		        }

		    }

		    output = Base64._utf8_decode(output);

		    return output;

		},

		// private method for UTF-8 encoding
		_utf8_encode : function (string) {
		    string = string.replace(/\r\n/g,"\n");
		    var utftext = "";

		    for (var n = 0; n < string.length; n++) {

		        var c = string.charCodeAt(n);

		        if (c < 128) {
		            utftext += String.fromCharCode(c);
		        }
		        else if((c > 127) && (c < 2048)) {
		            utftext += String.fromCharCode((c >> 6) | 192);
		            utftext += String.fromCharCode((c & 63) | 128);
		        }
		        else {
		            utftext += String.fromCharCode((c >> 12) | 224);
		            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
		            utftext += String.fromCharCode((c & 63) | 128);
		        }

		    }

		    return utftext;
		},

		// private method for UTF-8 decoding
		_utf8_decode : function (utftext) {
		    var string = "";
		    var i = 0;
		    var c = c1 = c2 = 0;

		    while ( i < utftext.length ) {

		        c = utftext.charCodeAt(i);

		        if (c < 128) {
		            string += String.fromCharCode(c);
		            i++;
		        }
		        else if((c > 191) && (c < 224)) {
		            c2 = utftext.charCodeAt(i+1);
		            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
		            i += 2;
		        }
		        else {
		            c2 = utftext.charCodeAt(i+1);
		            c3 = utftext.charCodeAt(i+2);
		            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
		            i += 3;
		        }

		    }

		    return string;
		}

	}

	return Base64;
}

function decryptBase64(cypherText){
	var key = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	var base64 = getBase64(key);
	var message = base64.decode(cypherText);
	return message;
}

function encryptBase64(message,key) {
	var key = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	var base64 = getBase64(key);
	var cypherText = base64.encode(message);
	return cypherText;
}

function encryptAES(message,key){
	var cypherText = CryptoJS.AES.encrypt(message, key);
	return cypherText.toString();
}

function decryptAES(cypherText, key){
	var message = CryptoJS.AES.decrypt(cypherText, key);
	return message.toString(CryptoJS.enc.Utf8);
}

function encryptSHA1(msg) {

  function rotate_left(n,s) {
    var t4 = ( n<<s ) | (n>>>(32-s));
    return t4;
  };
  function lsb_hex(val) {
    var str="";
    var i;
    var vh;
    var vl;
    for( i=0; i<=6; i+=2 ) {
      vh = (val>>>(i*4+4))&0x0f;
      vl = (val>>>(i*4))&0x0f;
      str += vh.toString(16) + vl.toString(16);
    }
    return str;
  };
  function cvt_hex(val) {
    var str="";
    var i;
    var v;
    for( i=7; i>=0; i-- ) {
      v = (val>>>(i*4))&0x0f;
      str += v.toString(16);
    }
    return str;
  };
  function Utf8Encode(string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";
    for (var n = 0; n < string.length; n++) {
      var c = string.charCodeAt(n);
      if (c < 128) {
        utftext += String.fromCharCode(c);
      }
      else if((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      }
      else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }
    }
    return utftext;
  };
  var blockstart;
  var i, j;
  var W = new Array(80);
  var H0 = 0x67452301;
  var H1 = 0xEFCDAB89;
  var H2 = 0x98BADCFE;
  var H3 = 0x10325476;
  var H4 = 0xC3D2E1F0;
  var A, B, C, D, E;
  var temp;
  msg = Utf8Encode(msg);
  var msg_len = msg.length;
  var word_array = new Array();
  for( i=0; i<msg_len-3; i+=4 ) {
    j = msg.charCodeAt(i)<<24 | msg.charCodeAt(i+1)<<16 |
    msg.charCodeAt(i+2)<<8 | msg.charCodeAt(i+3);
    word_array.push( j );
  }
  switch( msg_len % 4 ) {
    case 0:
      i = 0x080000000;
    break;
    case 1:
      i = msg.charCodeAt(msg_len-1)<<24 | 0x0800000;
    break;
    case 2:
      i = msg.charCodeAt(msg_len-2)<<24 | msg.charCodeAt(msg_len-1)<<16 | 0x08000;
    break;
    case 3:
      i = msg.charCodeAt(msg_len-3)<<24 | msg.charCodeAt(msg_len-2)<<16 | msg.charCodeAt(msg_len-1)<<8  | 0x80;
    break;
  }
  word_array.push( i );
  while( (word_array.length % 16) != 14 ) word_array.push( 0 );
  word_array.push( msg_len>>>29 );
  word_array.push( (msg_len<<3)&0x0ffffffff );
  for ( blockstart=0; blockstart<word_array.length; blockstart+=16 ) {
    for( i=0; i<16; i++ ) W[i] = word_array[blockstart+i];
    for( i=16; i<=79; i++ ) W[i] = rotate_left(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);
    A = H0;
    B = H1;
    C = H2;
    D = H3;
    E = H4;
    for( i= 0; i<=19; i++ ) {
      temp = (rotate_left(A,5) + ((B&C) | (~B&D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B,30);
      B = A;
      A = temp;
    }
    for( i=20; i<=39; i++ ) {
      temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B,30);
      B = A;
      A = temp;
    }
    for( i=40; i<=59; i++ ) {
      temp = (rotate_left(A,5) + ((B&C) | (B&D) | (C&D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B,30);
      B = A;
      A = temp;
    }
    for( i=60; i<=79; i++ ) {
      temp = (rotate_left(A,5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
      E = D;
      D = C;
      C = rotate_left(B,30);
      B = A;
      A = temp;
    }
    H0 = (H0 + A) & 0x0ffffffff;
    H1 = (H1 + B) & 0x0ffffffff;
    H2 = (H2 + C) & 0x0ffffffff;
    H3 = (H3 + D) & 0x0ffffffff;
    H4 = (H4 + E) & 0x0ffffffff;
  }
  var temp = cvt_hex(H0) + cvt_hex(H1) + cvt_hex(H2) + cvt_hex(H3) + cvt_hex(H4);

  return temp.toLowerCase();
}
